package com.logol.logolJava.controller;

import com.logol.logolJava.model.MdlPdfGenerate;
import com.logol.logolJava.utility.GeneratorEgatepass;
import com.logol.logolJava.utility.TestClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:9090")
@RestController
@RequestMapping("/api")
public class ApiController {

    @Value("${urlAccess}")
    private String urlAccess;


    @Autowired
    private GeneratorEgatepass generatorEgatepass;

    @Autowired
    private TestClass testClass;

    @Value("${PROP.FILE_UPLOAD_STORED}")
    private String fileLocation;

    @GetMapping
    public String testing(){
        String url = urlAccess;
        return testClass.test();
    }

    @PostMapping("/generate")
    public List<Map<String,String>> generatorEgatepass(@RequestBody List<MdlPdfGenerate> listData){
           List<Map<String, String>> listMap = new ArrayList<Map<String,String>>();
           try {

               for(MdlPdfGenerate mldPdf : listData) {
                   String url = generatorEgatepass.generateGatepassPdf(mldPdf);

                   Map<String, String> respEgateopass = new HashMap<String, String>();
                   respEgateopass.put("url", url);
                   respEgateopass.put("containerNumber", mldPdf.getContainerNumber());

                   listMap.add(respEgateopass);
               }

           } catch (Exception e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
           }
      return  listMap;

    }

    @GetMapping("/list")
    public ResponseEntity<List<String>> files() throws IOException {
        File dir = new File(fileLocation);
        List<String> collect = Arrays.stream(dir.listFiles())
                .filter(data -> data.isFile())
                .map(File::getName)
                .collect(Collectors.toList());
        return ResponseEntity.ok(collect);
    }
}
