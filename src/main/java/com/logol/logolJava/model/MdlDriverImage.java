package com.logol.logolJava.model;

import lombok.Data;

@Data
public class MdlDriverImage {

    public String vendorOrderDetailID;
    public String date;
    public String driverID;
    public String driverName;
    public String path;
    public String type;
    public String pathImage;
    public Integer imageID;
    public Boolean isDelete;
}
