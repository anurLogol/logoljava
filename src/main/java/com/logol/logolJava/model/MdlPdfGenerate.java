package com.logol.logolJava.model;

import lombok.Data;

@Data
public class MdlPdfGenerate {

    public String qrCode;
    public String gatepassType;
    public String containerNumber;
    public String containerStatus;
    public String isoCode;
    public String grossWeight;
    public String vesselName;
    public String voyageNo;
    public String etaDate;
    public String yardClosingTime;
    public String podSpod;
    public String customerName;
    public String bcDocument;
    public String imoCode;
}
