package com.logol.logolJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogolJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogolJavaApplication.class, args);
	}


}
