package com.logol.logolJava.utility;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.logol.logolJava.model.MdlPdfGenerate;
import com.logol.logolJava.model.MdlPdfProforma;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.naming.NamingException;

@Component
public class GeneratorEgatepass {

    public static Font regular = new Font(Font.FontFamily.HELVETICA, 7);
    public static Font bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
    public static Font boldLarge = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);

    public static Font labelRegular = new Font(Font.FontFamily.HELVETICA, 10);
    public static Font valueRegular = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);

    @Value("${urlAccess}")
    public String urlAccess;

    @Value("${env_separator}")
    public String env_separator;

    @Value("${PROP.FILE_UPLOAD_STORED}")
    public String env_file_location;

    @Value("${env_server_ipport}")
    public String env_server_ipport;

    @Value("${env_file_access}")
    public String env_file_access;

    @Value("${PROP.ENV_ASSET}")
    public String env_asset;

    public  String generateGatepassPdf(MdlPdfGenerate mldPdf ) throws Exception {

        System.out.println("urlAccess :: "+urlAccess);
        String serverFileSeparator = env_separator;//WebConfigHelper.getEnvVal("env_separator");
        String serverFileLocation = env_file_location;//WebConfigHelper.getEnvVal("env_file_location");
        String serverIpport = env_server_ipport; //WebConfigHelper.getEnvVal("env_server_ipport");
        String serverFileAccess = env_file_access;//WebConfigHelper.getEnvVal("env_file_access");

        System.out.println("serverFileLocation :: "+serverFileLocation);

        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year = localDate.getYear();
        String month = ConvertDateTimeHelper.formatDate(String.valueOf(localDate.getMonthValue()), "M", "MMM");
        int day = localDate.getDayOfMonth();
        String separator = serverFileSeparator;
        String destinationPath = serverFileLocation + "Gatepass" + separator + year + separator + month + separator
                + day + separator;
        String fileName = "ePass-" + mldPdf.getContainerNumber().replaceAll(" ", "_") + ".pdf";
        String finalPath = destinationPath + fileName;

        // initiate variable directory
        File directory = null;
        directory = new File(destinationPath.substring(0, destinationPath.length() - 1));
        System.out.println("destinationPath.substring(0, destinationPath.length() - 1)..."+destinationPath.substring(0, destinationPath.length() - 1));
        // check if directory is exist or not, if not, create it first


        if (!directory.exists()) {
            System.out.println("......!directory ........");
            directory.mkdirs();
        }else
        {
            System.out.println("......ada directory ........");
        }
        // opens an output stream to save into file
        FileOutputStream outputStream = new FileOutputStream(finalPath);

        try (OutputStream pdfStream = outputStream) {

            Document document = new Document();
            Rectangle pageSize = new Rectangle(192, 396);
            document.setPageSize(pageSize);
            document.setMargins(2, 2, 2, 2);

            PdfWriter.getInstance(document, pdfStream);

            document.open();

            // -- Header
            PdfPTable table = new PdfPTable(2);
            addTableHeader(table, mldPdf.qrCode);
            addRows(table, " ", " ");
            addRows(table, "Gatepass Type", ": " + mldPdf.gatepassType);
            addRows(table, "Container Number", ": " + mldPdf.containerNumber);
            addRows(table, "Container Status", ": " + mldPdf.containerStatus);
            addRows(table, "ISO Code", ": " + mldPdf.isoCode);
            addRows(table, "Gross Weight", ": " + mldPdf.grossWeight);
            addRows(table, "Vessel Name", ": " + mldPdf.vesselName);
            addRows(table, "Voyage No", ": " + mldPdf.voyageNo);

            if (mldPdf.gatepassType.equals("IMPORT")) {
                addRows(table, "paid Through Date", ": " + mldPdf.yardClosingTime);
            } else {
                addRows(table, "ETA Date", ": " + mldPdf.etaDate);
                addRows(table, "Yard Closing Time", ": " + mldPdf.yardClosingTime);
            }
            addRows(table, "Pod / Spod", ": " + mldPdf.podSpod);
            addRows(table, "Customer Name", ": " + mldPdf.customerName);
            addRows(table, "BC Document", ": " + mldPdf.bcDocument);
            addRows(table, "IMO Code", ": " + mldPdf.imoCode);
            addRows(table, " ", " ");
            // addRows(table, " ", " ");

            PdfPTable footerTable = addFooterTable();

            document.add(table);
            document.add(footerTable);
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        String linkLocal = serverIpport + serverFileAccess + "Gatepass" + "/" + year + "/" + month + "/" + day + "/"
                + fileName;
        return linkLocal;
    }


    private static void addRows(PdfPTable table, String leftText, String rightText) {
        Font regular = new Font(Font.FontFamily.HELVETICA, 7);

        PdfPCell rowLeft = new PdfPCell(new Paragraph(leftText, regular));
        rowLeft.setBorder(Rectangle.NO_BORDER);
        rowLeft.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        rowLeft.setPadding(4);
        table.addCell(rowLeft);

        PdfPCell rowRight = new PdfPCell(new Paragraph(rightText, regular));
        rowRight.setBorder(Rectangle.NO_BORDER);
        rowRight.setVerticalAlignment(Element.ALIGN_MIDDLE);
        rowLeft.setPadding(4);
        table.addCell(rowRight);

    }

    private  void addTableHeader(PdfPTable table, String qrCode) throws Exception {

        BufferedImage qrCodeImg = QRCodeHelper.generateQRCodeImage(qrCode);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(qrCodeImg, "png", baos);
        Image iTextImage = Image.getInstance(baos.toByteArray());
        iTextImage.setWidthPercentage(120);
        iTextImage.setAlignment(Element.ALIGN_CENTER);
        iTextImage.setBorder(2);
        iTextImage.setBorderColor(BaseColor.BLACK);

        String imageFolderPath = env_asset;//WebConfigHelper.getEnvVal("env_file_location");
        String npct1Logo = imageFolderPath + "npct1.png";
        // URL rightImageResource = classLoader.getResource("images/npct1.png");
        Path rightImagePath = Paths.get(npct1Logo);
        Image rightImg = Image.getInstance(rightImagePath.toAbsolutePath().toString());
        rightImg.setWidthPercentage(75);
        rightImg.setAlignment(Element.ALIGN_RIGHT);

        Font regular = new Font(Font.FontFamily.HELVETICA, 7);
        Font bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
        Font boldLarge = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);

        PdfPCell headerLeft = new PdfPCell();
        headerLeft.setBorder(Rectangle.NO_BORDER);

        // Paragraph p = new Paragraph("LKT763R/NPCT1", bold);
        // p.setAlignment(Element.ALIGN_CENTER);

        headerLeft.addElement(iTextImage);
        // headerLeft.addElement(p);

        PdfPCell headerRight = new PdfPCell();
        headerRight.setPaddingTop(4);
        // headerRight.setPaddingLeft(50);
        headerRight.setHorizontalAlignment(Element.ALIGN_RIGHT);
        headerRight.setBorder(Rectangle.NO_BORDER);

        Paragraph eTicket = new Paragraph("eTicket", bold);
        eTicket.setAlignment(Element.ALIGN_RIGHT);

        Paragraph gatepass = new Paragraph("GATEPASS", boldLarge);
        gatepass.setAlignment(Element.ALIGN_RIGHT);

        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        String printDate = dateTime.format(formatter);

        Paragraph date = new Paragraph(printDate, regular);
        date.setAlignment(Element.ALIGN_RIGHT);

        headerRight.addElement(rightImg);
        headerRight.addElement(eTicket);
        headerRight.addElement(gatepass);
        headerRight.addElement(date);

        table.addCell(headerLeft);
        table.addCell(headerRight);
    }

    private  PdfPTable addFooterTable() throws URISyntaxException, IOException, DocumentException {
        String imageFolderPath = env_asset;//WebConfigHelper.getEnvVal("env_file_location");
        String logolLogo = imageFolderPath + "logol_colored.png";
        Path rightImagePath = Paths.get(logolLogo);
        Image rightImg = Image.getInstance(rightImagePath.toAbsolutePath().toString());
        rightImg.setWidthPercentage(50);
        rightImg.setAlignment(Element.ALIGN_LEFT);

        PdfPTable footerTable = new PdfPTable(2);
        footerTable.setWidthPercentage(50);
        footerTable.setWidths(new int[] { 1, 2 });
        footerTable.setHorizontalAlignment(Element.ALIGN_RIGHT);
        // footerTable.setPaddingTop(22);

        Font bold = new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD);

        PdfPCell rowLeft = new PdfPCell(new Phrase("Printed By ", bold));
        rowLeft.setBorder(Rectangle.NO_BORDER);
        rowLeft.setHorizontalAlignment(Element.ALIGN_RIGHT);
        rowLeft.setVerticalAlignment(Element.ALIGN_MIDDLE);
        rowLeft.setPadding(2);
        footerTable.addCell(rowLeft);

        PdfPCell rowRight = new PdfPCell();
        rowRight.setBorder(Rectangle.NO_BORDER);
        rowRight.setHorizontalAlignment(Element.ALIGN_LEFT);
        rowRight.setVerticalAlignment(Element.ALIGN_MIDDLE);
        rowRight.setPadding(2);
        rowRight.addElement(rightImg);
        footerTable.addCell(rowRight);

        return footerTable;
    }
}
