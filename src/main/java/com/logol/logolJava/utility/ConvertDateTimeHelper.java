package com.logol.logolJava.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class ConvertDateTimeHelper {

    public static String formatDate(String date, String initDateFormat, String endDateFormat) {

        String parsedDate = "";

        try {
            Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
            SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
            parsedDate = formatter.format(initDate);
        } catch (Exception ex) {

        }

        return parsedDate;
    }

    public static String dateToString(Date date, DateFormat dateFormat) {
        if (date != null) {
            return dateFormat.format(date);
        }

        return null;
    }

    public static LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

}

