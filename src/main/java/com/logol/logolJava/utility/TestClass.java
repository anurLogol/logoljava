package com.logol.logolJava.utility;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;



@Component
public class TestClass {

    @Value("${urlAccess}")
    public String urlAccess;

    public String test(){
        String xx = "haha";
        return urlAccess;
    }
}
