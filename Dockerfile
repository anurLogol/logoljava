ARG JDK_VERSION=11-oraclelinux8
FROM openjdk:${JDK_VERSION}

ENV WORK_FOLDER=/usr/local/share/applications

LABEL maintener="Anur Priyanto <anur.priyanto@gmail.com>"

# Created user
RUN groupadd www-data && \
adduser -r -g www-data www-data

# Create folder & give access to read and write
ENV FILE_UPLOAD_STORED=/var/lib/spring-boot/data/
RUN mkdir -p ${FILE_UPLOAD_STORED} && \
chmod -R 777 ${FILE_UPLOAD_STORED}/

WORKDIR ${WORK_FOLDER}
USER www-data

ENV APPLICTION_PORT=80
ENV PROFILE=default
ENV DATABASE_USER=root
ENV DATABASE_PASSWORD=password
ENV ENV_FILE_LOCATION=/usr/local/share/applications/img/
ENV ENV_ASSETS=${FILE_UPLOAD_STORED}

ARG JAR_FILE="docker-springboot-0.0.1-SNAPSHOT.jar"
COPY --chown=www-data:www-data target/${JAR_FILE} ${WORK_FOLDER}/spring-boot.jar

ENTRYPOINT ["java", "-jar", "-Djava.security.egd=file:/dev/./urandom", "spring-boot.jar"]

CMD ["--server.port=${APPLICTION_PORT}", "--spring.profiles.active=${PROFILE}"]

EXPOSE ${APPLICTION_PORT}/tcp

HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl -f http://localhost:${APPLICTION_PORT}/actuator/health || exit 1

VOLUME ${ENV_ASSETS}
